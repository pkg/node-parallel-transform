# Installation
> `npm install --save @types/parallel-transform`

# Summary
This package contains type definitions for parallel-transform (https://github.com/mafintosh/parallel-transform).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/parallel-transform.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/parallel-transform/index.d.ts)
````ts
// Type definitions for parallel-transform 1.1
// Project: https://github.com/mafintosh/parallel-transform
// Definitions by: Daniel Cassidy <https://github.com/djcsdy>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node"/>

import { Transform, TransformOptions, TransformCallback } from "stream";

type OnTransform = (chunk: any, callback: TransformCallback) => void;

declare namespace ParallelTransform {
    interface Options extends TransformOptions {
        ordered?: boolean | undefined;
    }
}

declare const ParallelTransform: {
    (maxParallel: number, opts: ParallelTransform.Options | undefined | null, ontransform: OnTransform): Transform;
    (opts: ParallelTransform.Options | number | undefined | null, ontransform: OnTransform): Transform;
    (ontransform: OnTransform): Transform;
    new(maxParallel: number, opts: ParallelTransform.Options | undefined | null, ontransform: OnTransform): Transform;
    new(opts: ParallelTransform.Options | number | undefined | null, ontransform: OnTransform): Transform;
    new(ontransform: OnTransform): Transform;
};

export = ParallelTransform;

````

### Additional Details
 * Last updated: Wed, 07 Jul 2021 17:02:25 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Daniel Cassidy](https://github.com/djcsdy).
